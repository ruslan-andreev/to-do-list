let toDoList = function ToDoList(){

    let toDoArr = [];

    this.init = function(){ 
        if(localStorage.getItem('todo') == null){
            show(toDoArr);
        }else{
            toDoArr = JSON.parse(localStorage.getItem('todo'));
            show(toDoArr);
        } 
    }

    //  ввод по нажатию enter
    let input__keyup = document.querySelector('.input__task')
    input__keyup.addEventListener('keyup',function(event){
        if(event.keyCode == '13'){
            task = document.querySelector('.input__task').value;
        let input__field = document.querySelector('.input__task')
        input__field.value="";
        if(task == '') return;
        addTask(task);
        }
    })

    //ввод по click
    let input__btn = document.querySelector('.input__button');
    input__btn.addEventListener('click', function(){
        task = document.querySelector('.input__task').value;
        let input__field = document.querySelector('.input__task')
        input__field.value="";
        console.log(task)
        if(task == '') return;
        addTask(task);  
    })

    this.addTask = function(){
        let toDo = {
            todo: task,
            flag: true
        }
        
        if(localStorage.getItem('todo') == null){
            toDoArr.push(toDo);
            localStorage.setItem('todo', JSON.stringify(toDoArr))
            show(toDoArr);
        }else{
            toDoArr = JSON.parse(localStorage.getItem('todo'));
            toDoArr.push(toDo);
            localStorage.setItem('todo', JSON.stringify(toDoArr))
            show(toDoArr);
        }
    }
    this.show = function(toDoArr){
        ul = document.querySelector('.task__list')
        ul.innerHTML = ''; //очищает UL и выводит новый список

        toDoArr.forEach((item,index)=>{
            let li = document.createElement('li')
            li.classList.add('task__li')
            li.innerHTML = `<div><input id='${index}' type="checkbox" class='check'> ${item.todo}</div>
            <div li__btn><button class='task__edit' id='${index}'><i class="fa fa-pencil" aria-hidden="true"></i></button><button class='task__del' id='${index}'><i class="fa fa-trash" aria-hidden="true"></i></button></d>`;
            
            ul.appendChild(li); 
        })

        let taskDelete = document.querySelectorAll('.task__del')
        taskDelete.forEach((btn,index)=>{
            btn.addEventListener('click',()=>{
                let id = +index
            this.delete(id);
            })
        })

        let taskEdit = document.querySelectorAll('.task__edit')
        taskEdit.forEach((btnedit,index)=>{
            btnedit.addEventListener('click',()=>{
                let id = +index   
            this.edit (id);  
            })
        })
    }
    
    this.delete = function(id){
        toDoArr = JSON.parse(localStorage.getItem('todo'));
        toDoArr.splice(id,1);
        localStorage.setItem('todo', JSON.stringify(toDoArr))
        this.show(toDoArr);
    }

    this.edit = function(id){
        editNew = prompt('Внесите изменения')
        if(editNew == '' || editNew == null){
            this.show(toDoArr);
        }else{
        toDoArr = JSON.parse(localStorage.getItem('todo'));
        toDoArr[id].todo = editNew;

        localStorage.setItem('todo', JSON.stringify(toDoArr))
        this.show(toDoArr); 
        }
    }

    let clearAll__btn = document.querySelector('.del__all')
    clearAll__btn.addEventListener('click',()=>{
        let conf = confirm('Вы точно хотите очистить весь список дел?!');
            if(conf == true){
                toDoArr.length = 0;
                localStorage.clear();
                this.show(toDoArr);
            }else{this.show(toDoArr)}      
    })  
}
toDoList(),init();